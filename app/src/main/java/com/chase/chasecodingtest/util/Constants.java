package com.chase.chasecodingtest.util;

/**
 * Created by Debasis on 12/01/2017.
 */
public class Constants {

    public interface ApiMethods {
        String PREFERENCE_LOCATION = "com.chase.chasecodingtest.PREFERENCE_LOCATION";
    }

    public interface JsonKeys {
        String ERRORS = "errors";
        String MESSAGE = "message";
    }

    public interface ApiRequestId {
        int API_BASE_VALUE = 200;
    }
}
