package com.chase.chasecodingtest.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.chase.chasecodingtest.R;
import com.chase.chasecodingtest.adapter.IssPassTimeListAdapter;
import com.chase.chasecodingtest.callback.ISSPassTimeCallBack;
import com.chase.chasecodingtest.model.ISSPassTime;
import com.chase.chasecodingtest.network.DaggerNetworkComponent;
import com.chase.chasecodingtest.network.ISSPassTimeApiService;
import com.chase.chasecodingtest.network.NetworkComponent;
import com.chase.chasecodingtest.network.NetworkModule;
import com.chase.chasecodingtest.util.CommonUtils;
import com.chase.chasecodingtest.util.LocationProvider;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends BaseActivity implements ISSPassTimeCallBack, LocationProvider.LocationCallback {

    NetworkComponent networkComponent;
    @Inject
    ISSPassTimeApiService issPassTimeApiService;
    private CompositeSubscription subscriptions;
    private ProgressDialog mProgressDialog;
    private IssPassTimeListAdapter mIssPassTimeListAdapter;
    private LocationProvider mLocationProvider;
    public static final int PERMISSIONS_REQUEST_LOCATION = 100;
    private double currentLatitude;
    private double currentLongitude;
    private ISSPassTimeCallBack mIssPassTimeCallBack;
    private boolean isPermissionGranted;

    @Override
    public String getTag() {
        return MainActivity.class.getCanonicalName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        this.mLocationProvider = new LocationProvider(this, MainActivity.this, this);
        this.mIssPassTimeCallBack = this;
        this.networkComponent = DaggerNetworkComponent.builder().networkModule(new NetworkModule()).build();
        this.networkComponent.inject(this);
        this.subscriptions = new CompositeSubscription();
        RecyclerView passTimeListView = (RecyclerView) findViewById(R.id.iss_pass_time_listView);
        this.mIssPassTimeListAdapter = new IssPassTimeListAdapter();
        passTimeListView.setAdapter(this.mIssPassTimeListAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        passTimeListView.setLayoutManager(layoutManager);
        passTimeListView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        passTimeListView.addItemDecoration(itemDecoration);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("One moment please...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!CommonUtils.isNetworkAvailable(this)) {
            showErrorDialog("Please make sure you have proper internet connection!!");
        } else {
            if (isPermissionGranted) {
                getIssPassTime();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = true;
                } else {
                    isPermissionGranted = false;
                    showErrorDialog("ISSPassTime app requires location permission.Please restart application and allow permission in order to continue.");
                }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        subscriptions.unsubscribe();
    }

    @Override
    public void onHttpResponseError(Throwable exception) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        showErrorDialog(exception.getMessage());
    }

    @Override
    public void onHttpRequestComplete(ISSPassTime issPassTime) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        this.mIssPassTimeListAdapter.setPassTimeList(issPassTime.getResponse());
        this.mIssPassTimeListAdapter.notifyDataSetChanged();
    }

    /*
    check location permission in runtime,alerts user for the first time when app is launched
     */
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Prompt the user once explanation has been shown
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_LOCATION);


        } else {
            isPermissionGranted = true;
        }

    }

    /*
    method to get the current location of mobile phone and call the ISS Passtime API to get the data
     */
    private void getIssPassTime() {
        getCurrentLocation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Subscription subscription = issPassTimeApiService.getISSPassTime(mIssPassTimeCallBack,
                        String.valueOf(currentLatitude), String.valueOf(currentLongitude), "100");
                subscriptions.add(subscription);
            }
        }, 1000);
        //mProgressDialog.show();
    }

    private void getCurrentLocation() {
        if (mLocationProvider != null) {
            mLocationProvider.connect();
        }
    }

    @Override
    public void handleNewLocation(Location location) {
        this.currentLatitude = location.getLatitude();
        this.currentLongitude = location.getLongitude();
        Log.d(getTag(), "Lat : " + currentLatitude + " Lon : " + currentLongitude);
    }
}
