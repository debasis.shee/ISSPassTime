package com.chase.chasecodingtest.network;

import com.chase.chasecodingtest.BuildConfig;
import com.chase.chasecodingtest.callback.ISSPassTimeService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/*
Class to create singleton instance of the Retrofit connection object with required JSON conversion
or Observable factory(RxJava in this case)
 */
@Module
public class NetworkModule {
    @Provides
    @Singleton
    Retrofit provideCall() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.ISS_PASS_TIME_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ISSPassTimeService providesISSPassTimeService(
            Retrofit retrofit) {
        return retrofit.create(ISSPassTimeService.class);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ISSPassTimeApiService providesISSPassTimeApiService(
            ISSPassTimeService issPassTimeService) {
        return new ISSPassTimeApiService(issPassTimeService);
    }

}