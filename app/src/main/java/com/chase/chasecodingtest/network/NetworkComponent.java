package com.chase.chasecodingtest.network;


import com.chase.chasecodingtest.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/*
interface to create fully implemented class by using dependency injection,using Dagger2
 */
@Singleton
@Component(modules = {NetworkModule.class,})
public interface NetworkComponent {
    void inject(MainActivity mainActivity);
}