package com.chase.chasecodingtest.network;

import android.util.Log;

import com.chase.chasecodingtest.callback.ISSPassTimeCallBack;
import com.chase.chasecodingtest.callback.ISSPassTimeService;
import com.chase.chasecodingtest.model.ISSPassTime;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/*
This class implements the method of Retrofit service and does the job of fetching data or returning
 error by subscribing to RxJava Job schedulers and backgrounds thread which are in built inside the library
 */
public class ISSPassTimeApiService {
    private ISSPassTimeService mIssPassTimeService;
    private static final String TAG = ISSPassTimeApiService.class.getCanonicalName();

    public ISSPassTimeApiService(ISSPassTimeService issPassTimeService) {
        this.mIssPassTimeService = issPassTimeService;
    }

    public Subscription getISSPassTime(final ISSPassTimeCallBack issPassTimeCallBack, String query,
                                       String units, String appId) {
        return this.mIssPassTimeService.getISSPassTimes(query, units, appId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ISSPassTime>>() {
                    @Override
                    public Observable<? extends ISSPassTime> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ISSPassTime>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "HTTP Response complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                        issPassTimeCallBack.onHttpResponseError(e);
                    }

                    @Override
                    public void onNext(ISSPassTime issPassTime) {
                        issPassTimeCallBack.onHttpRequestComplete(issPassTime);
                    }
                });
    }
}