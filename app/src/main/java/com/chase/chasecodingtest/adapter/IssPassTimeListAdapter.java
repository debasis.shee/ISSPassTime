package com.chase.chasecodingtest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chase.chasecodingtest.R;
import com.chase.chasecodingtest.model.Response;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
This is the adapter class to bind and display ISS Pass time api data in a List.RecyclerView has been
used for better performance and smooth UI rendering while scrolling
 */
public class IssPassTimeListAdapter extends RecyclerView.Adapter<IssPassTimeListAdapter.ViewHolder> {
    private static final String TAG = IssPassTimeListAdapter.class.getCanonicalName();

    private List<Response> passTimeList;

    public List<Response> getPassTimeList() {
        return passTimeList;
    }

    public void setPassTimeList(List<Response> passTimeList) {
        this.passTimeList = passTimeList;
    }

    public IssPassTimeListAdapter() {
        this.passTimeList = new ArrayList<>();
    }

    @Override
    public IssPassTimeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pass_time_list_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IssPassTimeListAdapter.ViewHolder holder, int position) {
        Response response = passTimeList.get(position);
        //Log.d(TAG, "Duration :" + response.getDuration() + " Time :" + response.getRisetime());
        holder.txtDuration.setText("Duration : " + String.valueOf(response.getDuration()) + " seconds");
        DateFormat df = DateFormat.getDateTimeInstance();
        String passTime = df.format(new Date(response.getRisetime() * 1000));
        holder.txtPassTime.setText(passTime);
    }

    @Override
    public int getItemCount() {
        return this.passTimeList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDuration;
        public TextView txtPassTime;

        public ViewHolder(View view) {
            super(view);
            this.txtDuration = (TextView) view.findViewById(R.id.duration_textView);
            this.txtPassTime = (TextView) view.findViewById(R.id.time_textView);
        }
    }
}