package com.chase.chasecodingtest.callback;

import com.chase.chasecodingtest.model.ISSPassTime;

/*
Interface which behaves as call back to return error or data from the Web API
and pass it to the Activity
 */
public interface ISSPassTimeCallBack {
    void onHttpResponseError(Throwable exception);

    void onHttpRequestComplete(ISSPassTime issPassTime);
}