package com.chase.chasecodingtest.callback;

import com.chase.chasecodingtest.model.ISSPassTime;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/*
This is the Retrofit interface which defines the HTTP method type and parameters need to be passed
in order to get the data.The method returns an RxJava Observable to provide a reactive model,notifying
the UI about any changes in Data Model
 */
public interface ISSPassTimeService {

    @GET("iss-pass.json?")
    Observable<ISSPassTime> getISSPassTimes(@Query("lat") String latitude, @Query("lon") String longitude,
                                            @Query("n") String numberOfPass);
}