package com.chase.chasecodingtest;

import android.support.test.InstrumentationRegistry;
import android.test.AndroidTestCase;

import com.chase.chasecodingtest.model.Response;

import org.junit.Assert;
import org.junit.Test;

public class ISSResponseTest extends AndroidTestCase {

    @Test
    public void testResponse() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                Response response = new Response();
                response.setDuration(639);
                response.setRisetime(1512166699);

                Assert.assertEquals(response.getDuration().intValue(), 639);
                Assert.assertEquals(response.getRisetime().intValue(), 1512166699);
            }
        });
    }

}