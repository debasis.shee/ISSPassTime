package com.chase.chasecodingtest;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.chase.chasecodingtest.activity.BaseActivity;
import com.chase.chasecodingtest.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testShowErrorDialog() {
        BaseActivity baseActivity = Mockito.spy(BaseActivity.class);
        Mockito.doNothing().when(baseActivity).showErrorDialog("Error");
    }
}